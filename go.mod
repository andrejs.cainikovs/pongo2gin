module pongo2gin

go 1.16

require (
	github.com/flosch/pongo2/v5 v5.0.0
	github.com/gin-gonic/gin v1.7.7
)
